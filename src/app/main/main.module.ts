import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MainComponent } from "./main.component";
import { ToolbarComponent } from "../toolbar/toolbar.component";
import { MainRoutingModule } from "./main.routing.module";
import { DashboarModule } from "../dashboard/dashboard.module";

@NgModule({
  declarations: [
    MainComponent,
    ToolbarComponent,
  ],
  imports: [CommonModule, MainRoutingModule, DashboarModule],
})
export class MainModule {}