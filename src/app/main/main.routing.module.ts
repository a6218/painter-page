import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MainComponent } from "./main.component";
import { CommonModule } from "@angular/common";
import { DashboarModule } from "../dashboard/dashboard.module";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren:
          () => import('../dashboard/dashboard.module')
          .then((m) => DashboarModule),
      },
      { path: '**', redirectTo: '/dashboard' },
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}